package com.example.registration1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.viewpager.widget.ViewPager
import com.example.registration1.adapters.pageadapters
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private lateinit var tableLayout: TableLayout
    private lateinit var viewPager: ViewPager

    private lateinit var editTextTextEmailAddress5: EditText
    private lateinit var editTextTextPassword9: EditText
    private lateinit var editTextTextPassword10: EditText
    private lateinit var buttonRegisterActivity: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        TableLayout = findViewById(R.id.tablayout)
        ViewPager = findViewById(R.id.viewpage)

        viewPager.adapter = pageadapters (this)

        TabLayoutMediator(tableLayout, viewPager){tab, position ->
            tab.text = "Tab ${position + 1}"
        }.attach()


        init()
        registrationListeners()

    }
    private fun init() {
        editTextTextEmailAddress5 = findViewById(R.id.editTextTextEmailAddress5)
        editTextTextPassword9 = findViewById(R.id.editTextTextPassword9)
        editTextTextPassword10 = findViewById(R.id.editTextTextPassword10)
        buttonRegisterActivity = findViewById(R.id.buttonregister)
    }
    private fun registrationListeners() {
        buttonRegisterActivity.setOnClickListener {
            val email = editTextTextEmailAddress5.text.toString()
            val password = editTextTextPassword9.text.toString()
            val passwordAuthentication = editTextTextPassword10.text.toString()

            if (email.isEmpty() || password.isEmpty() || password != passwordAuthentication) {
                Toast.makeText(
                    this,
                    "OOPS Something is not correct, please try again",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
        }
    }

}