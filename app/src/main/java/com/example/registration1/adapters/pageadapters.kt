package com.example.registration1.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.registration1.Npage.first
import com.example.registration1.Npage.second

class pageadapters(activiy:first ) : FragmentStateAdapter(activiy) {
    override fun getItemCount() = 2
    }

    override fun createFragment(position: Int): Fragment {
        if (position == 0){
            return first()
        }
        return second()
    }
}